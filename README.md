# Packer

Packer Modern, Automated

## Uso

- Clone repositorio
- Verifique se a CLI da AWS está configurada corretamente

```bash
packer build -var=ami_name=test -var=aws_region=sa-east-1 templates/aws.json
```
